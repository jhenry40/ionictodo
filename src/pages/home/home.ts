import { Component } from '@angular/core';
import { LoadingController, Loading } from 'ionic-angular';
import { NavController } from 'ionic-angular';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/timeout';

import { LoginService } from '../../app/services/login-service/login-service'
import { TodoService } from '../../app/services/todo-service/todo-service';


@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  userName = "kitty"
  userPassword = "kitty123"
  isLoggedIn = false
  todoItems = []
  loadingLogin: Loading
  loadingTodoItems: Loading

  constructor(public navCtrl: NavController, private loadingController: LoadingController, private loginService: LoginService, private todoService: TodoService) {
    
  }

  onLogin() {
    this.showLoadingForLogin()

    this.loginService.login(this.userName, this.userPassword)
    .subscribe(
      this.onLoginCompleted.bind(this), 
      this.onLoginFailed.bind(this)
    ) 
  }

  onRefreshTodoItems() {
    this.todoItems = []
    this.showLoadingForTodoItems()
    this.todoService.getItems().subscribe(todoItems=>{
      this.todoItems = todoItems.items
      this.dismissLoadingForTodoItems()
    },
    err=>{
      this.dismissLoadingForTodoItems()
      console.log(err)
    })
  }

  onLoginCompleted(userToken: any) {
    this.dismissLoadingForLogin()

    console.log("userToken:" + userToken.id_token)
    if (userToken.id_token === undefined || userToken.id_token == null) {
      return
    }
    this.loginService.setAuthenticationToken(userToken.id_token)
    this.isLoggedIn = true
    this.onRefreshTodoItems()
  }

  onLoginFailed(err: any) {
    this.dismissLoadingForLogin()
    console.log('error:' + err)
  }

  showLoadingForLogin() {
    this.createLoadingForLogin()
    this.presentLoadingForLogin()
  }

  presentLoadingForLogin() {
    this.loadingLogin.present()
  }

  dismissLoadingForLogin() {
    this.loadingLogin.dismiss()
  }

  createLoadingForLogin() {
    this.loadingLogin = this.loadingController.create({content: "Logging in...."})
  }

  showLoadingForTodoItems() {
    this.createLoadingForTodoItems()
    this.presentLoadingForTodoItems()
  }

  presentLoadingForTodoItems() {
    this.loadingTodoItems.present()
  }

  dismissLoadingForTodoItems() {
    this.loadingTodoItems.dismiss()
  }

  createLoadingForTodoItems() {
    this.loadingTodoItems = this.loadingController.create({content: "Getting items...."})
  }
}
